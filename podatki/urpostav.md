# Urna postavka natakarjev

Podatki: [urpostav.csv](urpostav.csv)

## Opis

Meritve urne postavke na vzorcu 72 natakarjev in vzorcu 55 natakaric, ki imajo pet ali več let
izkušenj.

## Format

Baza podatkov s 127 meritvami dveh spremenljivk

* *spol* je nominalna spremenljivka, ki ima dve vrednosti: M=moški, Z=ženski spol.
* *postavka* je numerična zvezna spremenljivka, ki predstavlja urno postavko natakarjev (v
ameriških dolarjih)

## Raziskovalna domneva

Urna postavka natakarjev je višja.